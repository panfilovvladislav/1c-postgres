# vim:set ft=dockerfile:

FROM debian:jessie
MAINTAINER "Vladislav Panfilov" <mail@panfilov.me>

RUN apt-get update -qq && apt-get install -y locales nano apt-utils \
        && localedef -i ru_RU -c -f UTF-8 -A /usr/share/locale/locale.alias ru_RU.UTF-8

ENV LANG ru_RU.utf8
ENV LC_ALL ru_RU.utf8

RUN groupadd -r postgres && useradd -r -g postgres postgres
RUN mkdir /docker-entrypoint-initdb.d

ENV PG_MAJOR 9.4
ENV PG_VERSION 9.4.2-1
ENV PG_ARCH amd64

ENV DIST_1C_POSTGRES_9_4 ./dist
ADD ${DIST_1C_POSTGRES_9_4} /opt/

RUN apt-get install -y ssl-cert libossp-uuid16 libxslt1.1 ucf libgssapi-krb5-2 libldap-2.4-2 libedit2 libbsd0 libk5crypto3 libkeyutils1 libkrb5-3 libkrb5support0 libsasl2-2 libsasl2-modules libsasl2-modules-db
RUN dpkg -i /opt/*.deb

#RUN mkdir -p /var/run/postgresql && chown -R postgres /var/run/postgresql

ENV PGPATH /usr/lib/postgresql/${PG_MAJOR}/bin/
ENV PGDATA /var/lib/postgresql/data
ENV PATH $PGPATH:$PATH

RUN chown -R postgres:postgres $PGPATH

VOLUME /var/lib/postgresql/data

ADD docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE 5432

CMD ["postgres"]
